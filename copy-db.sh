source .env
echo '#!/bin/sh' > script.sh
echo ". $REMOTE_APP_DIR/.env" >> script.sh
echo 'mysqldump --no-tablespaces -u $DB_USERNAME -p$DB_PASSWORD $DB_DATABASE '$@'> /home/forge/backup.sql' >> script.sh
chmod +x script.sh
ssh -o "StrictHostKeyChecking no" $REMOTE_SERVER_IP "hostname"
scp script.sh $REMOTE_SERVER_IP:~/
ssh $REMOTE_SERVER_IP "./script.sh"
ssh $REMOTE_SERVER_IP "rm script.sh"
rm script.sh
scp $REMOTE_SERVER_IP:backup.sql backup.sql
ssh $REMOTE_SERVER_IP "rm backup.sql"
sed 's/\sDEFINER=`[^`]*`@`[^`]*`//g' -i backup.sql
mysql -u $DB_USERNAME -p$DB_PASSWORD $DB_DATABASE < backup.sql
rm backup.sql